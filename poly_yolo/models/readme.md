**The directory includes four models pretrained on cityscapes datasets.**

For the training, we used 2474 images, and for validation 497 images. 
It may be beneficial to fine-tune a model on your dataset when you want to create predictions on different data. 

**The IDs of classes are as follows:**

0. car
1. person
2. pole
3. traffic sign
4. motorcycle
5. bicycle
6. rider
7. ego vehicle
8. trailer
9. traffic light
10. truck
11. bus
12. caravan

Note, not all IDs are detected.